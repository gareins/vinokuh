#include "alg.h"

/*
  Knjiznjica za izracunavanje vrednosti potrebnih za regulacijo grelca
*/

struct temp_regulator_s {
  // calculated in _filter
  float t_avg;
  float t_diff;
  int height;
  int t_cal[N];

  // needed for _pid
  float t_target;
  float t_old_avg;
  float t_integral;

  // input and output of alg_
  int t_raw[N];
  int pwr_out;  
};

typedef struct temp_regulator_s temp_regulator_s;

float abs_f(float nonabs) {return nonabs < 0 ? -nonabs : nonabs;}

temp_regulator_s* as;
int i_curr, h_curr, p_curr, d_curr, h_curr, m_curr;

void alg_init( void ) {
  as = (temp_regulator_s*) calloc (1, sizeof(temp_regulator_s));
  as->t_target = T;
  p_curr = P;
  i_curr = I;
  d_curr = D;
  h_curr = H;
  m_curr = M;
}

void alg_pid( void )
{
  float fac, fac_p, fac_i, fac_d, fac_m, fac_h;

  // P
  fac_p = as->t_target - as->t_avg;
    fac_p = abs_f(fac_p)>P_MIN * p_curr*fac_p;

  // I
  as->t_integral = ( as->t_integral * (I_LEN-1) + (as->t_target - as->t_avg) ) / I_LEN;
  fac_i = as->t_integral;
  fac_i = abs_f(fac_i)>I_MIN * i_curr*fac_i;

  // D
  fac_d = as->t_avg - as->t_old_avg;
    fac_d = abs_f(fac_d)>D_MIN * d_curr*fac_d;
  as->t_old_avg = as->t_avg;

  // M
  fac_m = as->t_diff;
  fac_m = abs_f(fac_m)>M_MIN * m_curr*fac_m;

  // H
  fac_h = (float) (as->height * h_curr);

  // final calculation
  fac = (fac_p + fac_i + fac_d + fac_m) * fac_h ;
  as->pwr_out = (unsigned int) (fac < 0 ? 0/*raise?*/ : fac);
}

float ts_k[N] = {TS0_K, TS1_K, TS2_K, TS3_K, TS4_K, TS5_K, TS6_K};
float ts_t0[N] = {TS0_T0, TS1_T0, TS2_T0, TS3_T0, TS4_T0, TS5_T0, TS6_T0};

void alg_filter( void )
{
  int i;
  //take from as->t_raw and calculate t_avg, t_diff, height

  // step one: perform calibration
  for(i=0; i<N; i++)
    as->t_cal[i] = ((ts_k[i] * (float)(as->t_raw[i])) + ts_t0[i]);

  // height shold be between 0 - 7
  /* TODO: step two: try to determine height (should be the same as before or -1)
    idea:
     - delta_ij >> avg_delta => i=height
  */
  as->height = N; // TODO: delete after step two implementation

  // step three: determine t_avg and t_diff
  for(i=0; i<as->height; i++)
    as->t_avg += (float)as->t_cal[i];

  as->t_avg /= as->height;
}

int alg_calc( int* sensor_data )
{
  int i;
  for(i=0; i<N; i++)
    as->t_raw[i] = sensor_data[i];

  alg_filter();
  alg_pid();
  return as->pwr_out;
}

// setters and getters
void  alg_set_target( int temp )  {
  temp = (temp > T_MAX) ? T_MAX : temp;
  temp = (temp < T_MIN) ? T_MIN : temp;
  as->t_target = temp;
}

int   alg_get_height( void )      {return as->height;}
int   alg_get_curr_t( void )      {return as->t_avg;}
int   alg_get_target( void )      {return as->t_target;}
int   alg_get_t_cal( int ts_i )   {return (ts_i<N) ? as->t_cal[ts_i] : -1;}

void  alg_set_fac_p( float fac )    {p_curr = fac;}
void  alg_set_fac_i( float fac )    {i_curr = fac;}
void  alg_set_fac_d( float fac )    {d_curr = fac;}
void  alg_set_fac_h( float fac )    {h_curr = fac;}
void  alg_set_fac_m( float fac )    {m_curr = fac;}

int   alg_get_fac_p( void )       {return p_curr;}
int   alg_get_fac_i( void )       {return i_curr;}
int   alg_get_fac_d( void )       {return d_curr;}
int   alg_get_fac_h( void )       {return h_curr;}
int   alg_get_fac_m( void )       {return m_curr;}


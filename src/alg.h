#include <stm32f4xx.h>
#include <stdlib.h>

#include "ts.h"

#define T         60
#define T_MAX     90
#define T_MIN     20
#define P         10
#define P_MIN      5
#define I         10
#define I_MIN      5
#define D          0
#define D_MIN      0
#define M         -1
#define M_MIN      0

#define FAC_LIMIT 10

#define H       0.01   //scale factor - linear with height
#define N     TS_NUM   //number of sensors / max height
#define I_LEN     50   //sirina dolocenega integrala

/*      Factors:
 * @P   proportional,  odziv na oddaljenost trenutne temperature od koncne
 * @I   integralni,    odziv na zgodovino oddaljenosti od koncne temperature
 * @D   differential,  odziv na velikost sprembe temperature od zadnjega merjenja
 * @M   mixing,        odziv na razliko v temperaturi na razlicnih visinah
...:
- 0 none
- 1 50% 500W
- 2 500W
- 3 50% 500W, 50% 1000W
- 4 1000W
- 5 50% 1000W, 50% 1500W
- 6 1500W
*/

/* Calibration constants */

#define TS0_K     0.19873
#define TS0_T0  -18.17965

#define TS1_K     0.22116
#define TS1_T0  -19.68329

#define TS2_K     0.22255
#define TS2_T0  -19.04994

#define TS3_K     0.24101
#define TS3_T0  -21.36957

#define TS4_K     0.23917
#define TS4_T0  -34.99321

#define TS5_K     0.25188
#define TS5_T0  -24.92711

#define TS6_K     0.29145
#define TS6_T0  -31.99040

float   abs_f( float );
void    alg_init( void );
void    alg_pid( void );
void    alg_filter( void );
int     alg_calc( int* );
void    alg_set_target( int );
int     alg_get_height( void );
int     alg_get_curr_t( void );
int     alg_get_target( void );
int     alg_get_t_cal( int );

void    alg_set_fac_p( float );
void    alg_set_fac_i( float );
void    alg_set_fac_d( float );
void    alg_set_fac_h( float );
void    alg_set_fac_m( float );

int     alg_get_fac_p( void );
int     alg_get_fac_i( void );
int     alg_get_fac_d( void );
int     alg_get_fac_h( void );
int     alg_get_fac_m( void );

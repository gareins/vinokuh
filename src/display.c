#include "display.h"

/*
  Driver za prikaz podatkov preko LCD zaslona
*/

/* 
   ####################################################
   ##   Functions from or based on Grant Phillips's  ##
   ##   src : http://skrci.me/gf_disp                ##
	 ####################################################
*/

static char	        name[16]     = "display_driver";
static unsigned int min_run_time = OS_MILISECOND*20;
static unsigned int max_run_time = OS_MILISECOND*40;
static unsigned int min_priority = 100;
static unsigned int max_priority = 3;

void display_pin_init(void)
{
	/* Configure the peripheral clocks for the HD44780 data and control lines */
	RCC_AHB1PeriphClockCmd(HD44780_DATABIT4_CLK, ENABLE);
	RCC_AHB1PeriphClockCmd(HD44780_DATABIT5_CLK, ENABLE);
	RCC_AHB1PeriphClockCmd(HD44780_DATABIT6_CLK, ENABLE);
	RCC_AHB1PeriphClockCmd(HD44780_DATABIT7_CLK, ENABLE);

	RCC_AHB1PeriphClockCmd(HD44780_RS_CLK, ENABLE);
	RCC_AHB1PeriphClockCmd(HD44780_EN_CLK, ENABLE);

	/* Configure the HD44780 Data lines (DB7 - DB4) as outputs*/
	//gpio_init(HD44780_DATABITS, GPIO_Mode_OUT, GPIO_OType_PP, GPIO_PuPd_NOPULL, GPIO_Speed_50MHz, HD44780_DATAPORT);
	gpio_init(HD44780_DATABIT4, GPIO_Mode_OUT, GPIO_OType_PP, GPIO_PuPd_NOPULL, GPIO_Speed_50MHz, HD44780_DATABIT4_PORT);
	gpio_init(HD44780_DATABIT5, GPIO_Mode_OUT, GPIO_OType_PP, GPIO_PuPd_NOPULL, GPIO_Speed_50MHz, HD44780_DATABIT5_PORT);
	gpio_init(HD44780_DATABIT6, GPIO_Mode_OUT, GPIO_OType_PP, GPIO_PuPd_NOPULL, GPIO_Speed_50MHz, HD44780_DATABIT6_PORT);
	gpio_init(HD44780_DATABIT7, GPIO_Mode_OUT, GPIO_OType_PP, GPIO_PuPd_NOPULL, GPIO_Speed_50MHz, HD44780_DATABIT7_PORT);


	/* Configure the HD44780 Control lines (RS, RW, EN) as outputs*/
	//gpio_init(HD44780_CONTROLBITS, GPIO_Mode_OUT, GPIO_OType_PP, GPIO_PuPd_NOPULL, GPIO_Speed_50MHz, HD44780_CONTROLPORT);
	gpio_init(HD44780_RS_BIT, GPIO_Mode_OUT, GPIO_OType_PP, GPIO_PuPd_NOPULL, GPIO_Speed_50MHz, HD44780_RS_PORT);
	gpio_init(HD44780_EN_BIT, GPIO_Mode_OUT, GPIO_OType_PP, GPIO_PuPd_NOPULL, GPIO_Speed_50MHz, HD44780_EN_PORT);
	
	/* clear control bits */
	hd44780_EN_Off();
	hd44780_RS_Off();
	//hd44780_RW_Off();
	
	/* wait initial delay for LCD to settle */
	/* reset procedure - 3 function calls resets the device */
	delay_us(INIT_DELAY_1);
	display_wr_nibble( HD44780_CMD_RESET >> 4);
	delay_us(INIT_DELAY_2);
	display_wr_nibble( HD44780_CMD_RESET >> 4);
	delay_us(INIT_DELAY_3);
	display_wr_nibble( HD44780_CMD_RESET >> 4);
		
	/* 4bit interface */
	display_wr_nibble( HD44780_CMD_FUNCTION >> 4);

	/* sets the configured values - can be set again only after reset */
	display_wr_cmd( ( HD44780_CMD_FUNCTION | HD44780_CONF_BUS | HD44780_CONF_LINES | HD44780_CONF_FONT ) & 0x3F );

	/* turn the display on with no cursor or blinking */
	display_wr_cmd( ( HD44780_CMD_DISPLAY | HD44780_DISP_ON | HD44780_DISP_CURS_OFF | HD44780_DISP_BLINK_OFF ) & 0x0F );
	
	/* clear the display */
	display_wr_cmd( HD44780_CMD_CLEAR );
	
	/* addr increment, shift cursor */
	display_wr_cmd( ( HD44780_CMD_ENTRY_MODE | HD44780_ENTRY_ADDR_INC | HD44780_ENTRY_SHIFT_CURS ) & 0x07 );
}

queue_item queue[ QUEUE_LEN ];
int queue_last = 0;   // first free space after queue
int queue_first = 0;  // first item to execute on queue

void display_init()
{	
	// device specific
	display_pin_init();
	
	// platform specific
	addDriver(name, display_driver, min_run_time, max_run_time, min_priority, max_priority);
}

void display_wr_nibble( unsigned char data )
{
	if ( data & 0x01 ) {
		GPIO_SetBits( HD44780_DATABIT4_PORT, HD44780_DATABIT4 );
	} else {
		GPIO_ResetBits( HD44780_DATABIT4_PORT, HD44780_DATABIT4 );
	}
	if ( data & 0x02 ) {
		GPIO_SetBits( HD44780_DATABIT5_PORT, HD44780_DATABIT5 );
	} else {
		GPIO_ResetBits( HD44780_DATABIT5_PORT, HD44780_DATABIT5 );
	}
	if ( data & 0x04 ) {
		GPIO_SetBits( HD44780_DATABIT6_PORT, HD44780_DATABIT6 );
	} else {
		GPIO_ResetBits( HD44780_DATABIT6_PORT, HD44780_DATABIT6 );
	}
	if ( data & 0x08 ) {
		GPIO_SetBits( HD44780_DATABIT7_PORT, HD44780_DATABIT7 );
	} else {
		GPIO_ResetBits( HD44780_DATABIT7_PORT, HD44780_DATABIT7 );
	}

	/* set the EN signal */
	hd44780_EN_On();
	delay_us(WRITE_DELAY);
	
	/* unset EN signal and wait a bit */
	hd44780_EN_Off();
	delay_us(WRITE_DELAY_SHORT);
}

void display_wr_cmd( unsigned char data )
{
	display_wr_nibble(data >> 4);	
	display_wr_nibble(data);
}

void display_putch(char c)
{
	queue_item qi;	
	qi.type = _dat;
	qi.data = c;
	
	add_to_queue(qi);
}

void display_putstr(char* str)
{
	int i;
	for( i=0; str[i] && i<MAX_STR_LEN; i++)
		display_putch(str[i]);

	// TODO: rotate string if too long for one line
}

void display_clrscr()
{
	display_send_cmd(HD44780_CMD_CLEAR);
}

void display_goto(unsigned char x, unsigned char y)
{
  unsigned char copy_y=0;
	queue_item qi;
	
  if(x > (HD44780_DISP_LENGTH-1))
		x = 0;

	if(y > (HD44780_DISP_ROWS-1))
		y = 0;

	switch(y)
  {
		case 0:  copy_y = 0x80; break;
		case 1:  copy_y = 0xc0; break;
		case 2:  copy_y = 0x94; break;
		case 3:  copy_y = 0xd4; break;
  }
	
	qi.type = _cmd;
	qi.data = (char)(x + copy_y);
	add_to_queue(qi);
}

void display_send_cmd(char c)
{
	queue_item qi;
	qi.data = c;
	qi.type = _cmd;
	
	add_to_queue(qi);
}

void add_to_queue(queue_item qi)
{
	if ( queue_first == (queue_last+1)%QUEUE_LEN )
	{
		raise_error(5);
		return;
	}
	
	queue[queue_last] = qi;
	queue_last = (queue_last+1)%QUEUE_LEN;
}

int display_driver_helper()
{
	queue_item qi;
	if( queue_first == queue_last )
		return 1;
	
	qi = queue[queue_first];
	if ( qi.type == _dat )
		hd44780_RS_On();
	else
		hd44780_RS_Off();
	
	display_wr_cmd(qi.data);
	
	queue_first = (queue_first+1)%QUEUE_LEN;
	return 0;
}

void display_driver()
{
	int cmds = CMDS_PER_PERIOD;
	int i, tmp = 0;
	
	for( i=0; tmp != 1 && i < cmds; i++ )
		tmp = display_driver_helper();
}

void display_flush()
{
	while(queue_first != queue_last)
		display_driver();
}

void get_queue_status( int* first, int* last )
{
	*first = queue_first;
	*last  = queue_last;
	return;
}

// ******** String funcs *********

void int2str(char *str, int INT, char dol)	
{
	//Vpisi array v katerega se bo string shranil, nato int in nato dolzino izpisa
	//Ce je dolzina izpisa dolocena na nic, se bo nastavila sama
	int i, d, minus;
	
	minus = INT < 0;
	if( minus )
		INT = -INT;
	
	if(INT > 10e8)
	{
		if(dol)
		{
			for(i=0;i<dol;i++)
				str[i] = 'E';
			str[i] = 0;
		}
		else
			strcpy(str, "E");
		
		return;
	}
	
	if(dol) d = dol;
	else
	{
		d = 1 + minus;
		i = 10;
	
		while(1)
		{
			if(INT < i)
				break;
			
			d++;
			i *= 10;

		}
	}
	
	if( minus )
		str[0] = '-';

	str[d] = 0;
	for(i=0; i < d-minus; i++)
	{
		str[d-i-1] = (INT % 10) + '0';
		INT /= 10;
	}

}

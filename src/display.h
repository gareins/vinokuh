#include <stm32f4xx.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include "os.h"
#include "error.h"

/***************************************************************
 *
 * CONSTANTS
 *
 ***************************************************************/

/* COMMANDS */
#define HD44780_CMD_RESET            	    0x30      /*!< Resets display - used in init 3x */
#define HD44780_CMD_CLEAR            	    0x01      /*!< Clears display */
#define HD44780_CMD_RETURN_HOME      	    0x02      /*!< Sets DDRAM pointer to 0 */
#define HD44780_CMD_ENTRY_MODE       	    0x04      /*!< Sets how the pointer is updated after a character write */
#define HD44780_CMD_DISPLAY          	    0x08      /*!< Display settings */
#define HD44780_CMD_SHIFT            	    0x10      /*!< Cursor and display movement */
#define HD44780_CMD_FUNCTION         	    0x20      /*!< Screen type setup */
#define HD44780_CMD_CGRAM_ADDR       	    0x40      /*!< Sets CGRAM address */
#define HD44780_CMD_DDRAM_ADDR       	    0x80      /*!< Sets DDRAM address */

/* ENTRY_MODE Command parameters */
#define HD44780_ENTRY_SHIFT_DISP 			0x01      /*!< Shift display */
#define HD44780_ENTRY_SHIFT_CURS 			0x00      /*!< Shift cursor */
#define HD44780_ENTRY_ADDR_INC   			0x02      /*!< Increments pointer */
#define HD44780_ENTRY_ADDR_DEC   			0x00      /*!< Decrements pointer */

/* DISPLAY Command parameters */
#define HD44780_DISP_ON       				0x04      /*!< Enables the display */
#define HD44780_DISP_OFF      				0x00      /*!< Disables the display */
#define HD44780_DISP_CURS_ON  				0x02      /*!< Enables cursor */
#define HD44780_DISP_CURS_OFF 				0x00      /*!< Disables cursor */
#define HD44780_DISP_BLINK_ON			    0x01      /*!< Enables cursor blinking */
#define HD44780_DISP_BLINK_OFF  			0x00      /*!< Disables cursor blinking */

/* SHIFT Command parameters */
#define HD44780_SHIFT_DISPLAY    			0x08      /*!< Shifts the display or shifts the cursor if not set */
#define HD44780_SHIFT_CURSOR    			0x00      /*!< Shifts the display or shifts the cursor if not set */
#define HD44780_SHIFT_RIGHT      			0x04      /*!< Shift to the right */
#define HD44780_SHIFT_LEFT      			0x00      /*!< Shift to the left  */

/* FUNCTION Command parameters */
#define HD44780_FUNC_BUS_8BIT  				0x10      /*!< 8 bit bus */
#define HD44780_FUNC_BUS_4BIT  				0x00      /*!< 4 bit bus */
#define HD44780_FUNC_LINES_2   				0x08      /*!< 2 lines */
#define HD44780_FUNC_LINES_1   				0x00      /*!< 1 line */
#define HD44780_FUNC_FONT_5x10 				0x04      /*!< 5x10 font */
#define HD44780_FUNC_FONT_5x8  				0x00      /*!< 5x8 font */

/* Busy Flag - actually not used */
#define HD44780_BUSY_FLAG        			0x80      /*!< Busy flag */


/***************************************************************
 *
 * CONFIGURATION
 *
 ***************************************************************/

#define HD44780_CONF_BUS                    HD44780_FUNC_BUS_4BIT
#define HD44780_CONF_LINES                  HD44780_FUNC_LINES_2
#define HD44780_CONF_FONT                   HD44780_FUNC_FONT_5x8

#define HD44780_DISP_LENGTH					16
#define HD44780_DISP_ROWS					2
#define HD44780_CONF_SCROLL_MS				20

#define NO_USB_MUTLTIPLIER                  6
#define INIT_DELAY_1                        16000
#define INIT_DELAY_2                        5000
#define INIT_DELAY_3                        150
#define WRITE_DELAY                         100*NO_USB_MUTLTIPLIER
#define WRITE_DELAY_SHORT                   10*NO_USB_MUTLTIPLIER

#define QUEUE_LEN                           70
#define CMDS_PER_PERIOD                     (OS_PERIOD_TIME*1000/2) / (2*(WRITE_DELAY+WRITE_DELAY_SHORT))
#define MAX_STR_LEN                         16

/* HD44780 Data lines */
#define HD44780_DATABIT4                    GPIO_Pin_1
#define HD44780_DATABIT4_PORT               GPIOD
#define HD44780_DATABIT4_CLK                RCC_AHB1Periph_GPIOD

#define HD44780_DATABIT5					GPIO_Pin_12
#define HD44780_DATABIT5_PORT               GPIOC
#define HD44780_DATABIT5_CLK                RCC_AHB1Periph_GPIOC

#define HD44780_DATABIT6					GPIO_Pin_5
#define HD44780_DATABIT6_PORT               GPIOD
#define HD44780_DATABIT6_CLK                RCC_AHB1Periph_GPIOD

#define HD44780_DATABIT7					GPIO_Pin_3
#define HD44780_DATABIT7_PORT               GPIOD
#define HD44780_DATABIT7_CLK                RCC_AHB1Periph_GPIOD

/* HD44780 Control lines */
#define HD44780_RS_BIT						GPIO_Pin_10
#define HD44780_RS_PORT                     GPIOC
#define HD44780_RS_CLK                      RCC_AHB1Periph_GPIOC

#define HD44780_EN_BIT						GPIO_Pin_10
#define HD44780_EN_PORT                     GPIOA
#define HD44780_EN_CLK                      RCC_AHB1Periph_GPIOA

/***************************************************************
 *
 * STRUCTURES
 *
 ***************************************************************/

typedef enum {_cmd, _dat} item_type; 

struct queue_item {
	item_type type;
	char      data;	
};
typedef struct queue_item queue_item;

/***************************************************************
 *
 * FUNCTIONS
 *
 ***************************************************************/

#define hd44780_RS_On()		               GPIO_WriteBit(HD44780_RS_PORT, HD44780_RS_BIT, Bit_SET)     
#define hd44780_RS_Off()	           	   GPIO_WriteBit(HD44780_RS_PORT, HD44780_RS_BIT, Bit_RESET)
#define hd44780_EN_On()		               GPIO_WriteBit(HD44780_EN_PORT, HD44780_EN_BIT, Bit_SET)
#define hd44780_EN_Off()	               GPIO_WriteBit(HD44780_EN_PORT, HD44780_EN_BIT, Bit_RESET)

void display_pin_init(void);
void display_init( void );

void display_driver( void );
int  display_driver_helper( void );

void display_putch( char c );
void display_clrscr( void );
void display_putstr( char* str );
void display_goto( unsigned char x, unsigned char y );

void get_queue_status( int* first, int* last );
void add_to_queue( queue_item qi );

void display_wr_nibble( unsigned char data );
void display_wr_cmd( unsigned char data );
void display_send_cmd( char c );
void display_flush( void );

void int2str(char *str, int INT, char dol);

#include "encoder.h"

/*
  Driver za vnos podatkov preko enkoderja
  Branja vrtenja enkoderja nama ni uspelo implementirati, zato se tudi ta driver ne uporablja
*/

// **************** INIT ******************

static char              name[16] = "encoder_driver";
static unsigned int min_run_time = OS_MILISECOND*200;
static unsigned int max_run_time = OS_MILISECOND*50;
static unsigned int min_priority = 50;
static unsigned int max_priority = 3;

void encoder_init() 
{
/*
 * Configure two timers as quadrature encoder counters. 
 * Details of which timers should be used are
 * in the project hardware header file.
 * Most timers can be used if channels 1 and 2 are available on pins.
 * The timers are mostly 16 bit. Timers can be set to 32 bit but they are
 * not very convenient for IO pins so the counters are simply set to to
 * 16 bit counting regardless.
 * A mouse needs 32 bits of positional data and, since it also needs the
 * current speed, distance is not maintained by the encoder code but will
 * be looked after by the motion control code.
 * The counters are set to X4 mode. The only alternative is X2 counting.
 */
	
  // turn on the clocks for each of the ports needed
  RCC_AHB1PeriphClockCmd (ENC_GPIO_CLK, ENABLE);
	RCC_APB1PeriphClockCmd (ENC_TIMER_CLK, ENABLE);

  // now configure the pins themselves
  // they are all going to be inputs with pullups
	gpio_init(
		ENC_PIN_A | ENC_PIN_B,
		GPIO_Mode_AF,
		GPIO_OType_PP,
		GPIO_PuPd_NOPULL,
		GPIO_Speed_100MHz,
		ENC_GPIO_PORT
	);
	
  // Connect the pins to their Alternate Functions
  GPIO_PinAFConfig (ENC_GPIO_PORT, ENC_SOURCE_A, ENC_AF);
  GPIO_PinAFConfig (ENC_GPIO_PORT, ENC_SOURCE_B, ENC_AF);

  // set them up as encoder inputs
  // set both inputs to rising polarity to let it use both edges
  TIM_EncoderInterfaceConfig (ENC_TIMER, TIM_EncoderMode_TI12, 
                              TIM_ICPolarity_Rising, 
                              TIM_ICPolarity_Rising);
  TIM_SetAutoreload (ENC_TIMER, 0xffff);

  // turn on the timer/counters
  TIM_Cmd (ENC_TIMER, ENABLE);
	
  encoder_reset();

  // OS specific
  addDriver(name, encoder_driver, min_run_time, max_run_time, min_priority, max_priority);
}

//available to the rest of the code
//speeds
int16_t old_value;
int16_t value;
int16_t diff_value;
int16_t enc_status;

void encoder_reset (void)
{
  __disable_irq();
  old_value = 0;
  value = 0;
  diff_value = 0;
  TIM_SetCounter (ENC_TIMER, 0);
	
  encoder_driver();
  __enable_irq();
}

void encoder_info(int* a, int* b, int* c, int* d)
{
	*a = old_value;
	*b = value;
	*c = diff_value;
	*d = enc_status;
	return;
}

int encoder_read()
{
	int toRet = enc_status;
	enc_status = 0;
	return toRet;
}

void encoder_driver (void)
{
	old_value = value;
    value = TIM_GetCounter (ENC_TIMER) ;
	diff_value = old_value - value ;
	
	if ( diff_value > 0  )
		enc_status ++;
	else if ( diff_value < 0 )
		enc_status --;
}

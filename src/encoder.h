#include <stm32f4xx.h>
#include <string.h>

#include "os.h"
#include "error.h"

// Pins
#define ENC_PIN_A               GPIO_Pin_6
#define ENC_SOURCE_A            GPIO_PinSource6
#define ENC_PIN_B               GPIO_Pin_8
#define ENC_SOURCE_B            GPIO_PinSource8

// Port, Clock and Timers
#define ENC_AF                  GPIO_AF_TIM4
#define ENC_TIMER               TIM4
#define ENC_TIMER_CLK           RCC_APB1Periph_TIM4
#define ENC_GPIO_CLK            RCC_AHB1Periph_GPIOB
#define ENC_GPIO_PORT           GPIOB

#define ENC_COUNT()             ENC_TIMER->CNT

void encoder_reset( void );
void encoder_init( void );

int encoder_read( void );
void encoder_driver( void );
void encoder_info(int* a, int* b, int* c, int* d);


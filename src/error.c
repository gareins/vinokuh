#include "error.h"

/*
	Knjiznjica za sporocanje napak preko priziganja ledic na ploscici in pokrovu
*/

unsigned int NO_ERROR = 1;

static char				 name[16] = "error_driver";
static unsigned int min_run_time = OS_SECOND;
static unsigned int max_run_time = OS_SECOND*2;
static unsigned int min_priority = 100;
static unsigned int max_priority = 2;

void err_led_init() {
	//platform specific
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);	
	gpio_init(ERR_LED_ALL, GPIO_Mode_OUT, GPIO_OType_PP, GPIO_PuPd_NOPULL, GPIO_Speed_100MHz,  GPIOD);
	
	//OS specific
	addDriver(name, error_driver, min_run_time, max_run_time, min_priority, max_priority);
}

void error_driver(void) {
	if( getOsErrno() != 0)
	{
		raise_error(getOsErrno()); 
		NO_ERROR = 0;
	}
	if( NO_ERROR )
		toggle_err_led(ERR_LED_GOOD);
}

void raise_error(int errno) {
	/* Raise error 0-15
	 * bits = [green orange blue red]
	 * ex.10 = [ 1 0 1 0 ]
	* Do not use errno: 0 and 8 !!
	*/
	int i;
	
	clr_err_led(ERR_LED_ALL);
	
	if(errno&0x1)
		set_err_led(ERR_LED_2);
	if(errno&0x2)
		set_err_led(ERR_LED_3);
	if(errno&0x4)
		set_err_led(ERR_LED_1);
	if(errno&0x8)
		set_err_led(ERR_LED_0);
	
	//disable interrupts!
	NO_ERROR = 0;

	// warn the user with outer leds and cycle
	for(i=0;;i=(i+1)%2) {
		if(i)
			led_error_warning(errno);
		else
			led_clr_all();
		delay_ms(300);
	}	
}

void set_err_led(unsigned int led) {
	GPIO_SetBits(GPIOD, led); // Spremeni stanje na LED
}
void clr_err_led(unsigned int led) {
	GPIO_ResetBits(GPIOD, led); // Spremeni stanje na LED
}
void toggle_err_led(unsigned int led) {
	GPIO_ToggleBits(GPIOD, led); // Spremeni stanje na LED
}

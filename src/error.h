#include <stm32f4xx.h>

#include "gpio.h"
#include "os.h"
#include "led.h"

#define ERR_LED_0 GPIO_Pin_12
#define ERR_LED_1 GPIO_Pin_13
#define ERR_LED_2 GPIO_Pin_14
#define ERR_LED_3 GPIO_Pin_15

#define ERR_LED_GREEN  GPIO_Pin_12
#define ERR_LED_ORANGE GPIO_Pin_13
#define ERR_LED_RED    GPIO_Pin_14
#define ERR_LED_BLUE   GPIO_Pin_15

#define ERR_LED_GOOD ERR_LED_0
#define ERR_LED_ALL (ERR_LED_0|ERR_LED_1|ERR_LED_2|ERR_LED_3)

void err_led_init(void);
void error_driver(void);
void raise_error(int errno);

void set_err_led(unsigned int);
void clr_err_led(unsigned int);
void toggle_err_led(unsigned int);


#include "gpio.h"

/*
	Knjiznjica za upravljanje s pini.
	Vsebuje samo eno, ampak zelo uporabno funkcijo, ki jo klicemo v vseh ostalih initih.
*/

void gpio_init(	uint16_t GPIO_Pins,						
				GPIOMode_TypeDef 	GPIO_Mode,  	
				GPIOOType_TypeDef 	GPIO_OType, 
				GPIOPuPd_TypeDef 	GPIO_PuPd,	
 				GPIOSpeed_TypeDef 	GPIO_Speed, 
				GPIO_TypeDef* 		GPIO_abcd) 
{
	GPIO_InitTypeDef gpioT;
	GPIO_StructInit(&gpioT);
	
	gpioT.GPIO_Pin = GPIO_Pins;
	gpioT.GPIO_Speed = GPIO_Speed;
	gpioT.GPIO_Mode = GPIO_Mode;
	gpioT.GPIO_OType = GPIO_OType;
	gpioT.GPIO_PuPd = GPIO_PuPd;
	GPIO_Init(GPIO_abcd, &gpioT); 
}

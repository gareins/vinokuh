#include <stm32f4xx.h>

void gpio_init( uint16_t GPIO_Pins,						
				GPIOMode_TypeDef     GPIO_Mode,  	
				GPIOOType_TypeDef    GPIO_OType, 
				GPIOPuPd_TypeDef     GPIO_PuPd,	
                GPIOSpeed_TypeDef    GPIO_Speed, 
				GPIO_TypeDef*        GPIO_abcd);

#include "input.h"

/*
  Driver za vnos podatkov preko gumbov
*/

static char             name[16] = "input_driver";
static unsigned int min_run_time = OS_MILISECOND*100;
static unsigned int max_run_time = OS_MILISECOND*100;
static unsigned int min_priority = 30;
static unsigned int max_priority = 5;

int last_stat[NUM_BTNS];
int press_up[NUM_BTNS];
int press_down[NUM_BTNS];

int in_pins[NUM_BTNS] = {IN_L, IN_C, IN_R};

void input_driver( void )
{
    int i, new_stat;
    for(i=0;i<NUM_BTNS;i++) {      
      new_stat = (IN_PORT->IDR & in_pins[i]); //read hardware in_pins[i]
      if( new_stat != last_stat[i] ) {
        press_down[i] = new_stat;
        press_up[i] = !new_stat;
        last_stat[i] = new_stat;
      }
    }
}

void input_init( void ) 
{
    last_stat[0] = 0;
    last_stat[1] = 0;
    last_stat[2] = 0;

    gpio_init(IN_PINS,GPIO_Mode_IN, GPIO_OType_PP, GPIO_PuPd_DOWN, GPIO_Speed_100MHz, IN_PORT);

    addDriver(name, input_driver, min_run_time, max_run_time, min_priority, max_priority);
}

int input_is_press_up(int btn) {
  int toRet = press_up[btn];
  press_up[btn] = 0;
  return toRet;
}

int input_is_press_down(int btn) {
  int toRet = press_down[btn];
  press_down[btn] = 0;
  return toRet;
}

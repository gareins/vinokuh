#include <stm32f4xx.h>

#include "os.h"
#include "gpio.h"

#define IN_L     GPIO_Pin_4
#define IN_C     GPIO_Pin_2
#define IN_R     GPIO_Pin_0
#define IN_PINS  (IN_L | IN_C | IN_R)

#define IN_PORT  GPIOE

#define BTN_L       0
#define BTN_C       1
#define BTN_R       2

#define NUM_BTNS    3

int     get_button_pressed( void );
void    input_init( void );
void    input_driver( void );
int     input_is_press_up( int btn );
int     input_is_press_down( int btn );

#include "led.h"

/*
    Driver za LED-ice
*/

static char              name[16] = "led_driver";
static unsigned int min_run_time = OS_SECOND;
static unsigned int max_run_time = OS_SECOND*2;
static unsigned int min_priority = 100;
static unsigned int max_priority = 5;

int green_height, red_height;

int green_pins[] = {LED_GREEN_0, LED_GREEN_1, LED_GREEN_2, LED_GREEN_3, LED_GREEN_4, LED_GREEN_5, LED_GREEN_6};
GPIO_TypeDef* green_ports[] = {LED_GREEN_0_PORT, LED_GREEN_1_PORT, LED_GREEN_2_PORT, LED_GREEN_3_PORT, LED_GREEN_4_PORT, LED_GREEN_5_PORT, LED_GREEN_6_PORT};

int red_pins[] = {LED_RED_0, LED_RED_1, LED_RED_2, LED_RED_3, LED_RED_4, LED_RED_5, LED_RED_6};
GPIO_TypeDef* red_ports[] = {LED_RED_0_PORT, LED_RED_1_PORT, LED_RED_2_PORT, LED_RED_3_PORT, LED_RED_4_PORT, LED_RED_5_PORT, LED_RED_6_PORT};

void led_init() {
    //platform specific
    green_height = red_height = 0;

    if(LED_GPIOA_PINS != LED_NO_PIN_ON_PORT) {
        RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
        gpio_init(LED_GPIOA_PINS, GPIO_Mode_OUT, GPIO_OType_PP, GPIO_PuPd_NOPULL, GPIO_Speed_100MHz, GPIOA);
    }
    if(LED_GPIOB_PINS != LED_NO_PIN_ON_PORT) {
        RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
        gpio_init(LED_GPIOB_PINS, GPIO_Mode_OUT, GPIO_OType_PP, GPIO_PuPd_NOPULL, GPIO_Speed_100MHz, GPIOB);
    }
    if(LED_GPIOC_PINS != LED_NO_PIN_ON_PORT) {
        RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
        gpio_init(LED_GPIOC_PINS, GPIO_Mode_OUT, GPIO_OType_PP, GPIO_PuPd_NOPULL, GPIO_Speed_100MHz, GPIOC);
    }
    if(LED_GPIOD_PINS != LED_NO_PIN_ON_PORT) {
        RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
        gpio_init(LED_GPIOD_PINS, GPIO_Mode_OUT, GPIO_OType_PP, GPIO_PuPd_NOPULL, GPIO_Speed_100MHz, GPIOD);
    }
    if(LED_GPIOE_PINS != LED_NO_PIN_ON_PORT) {
        RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOE, ENABLE);
        gpio_init(LED_GPIOE_PINS, GPIO_Mode_OUT, GPIO_OType_PP, GPIO_PuPd_NOPULL, GPIO_Speed_100MHz, GPIOE);
    }
    
    //OS specific
    addDriver(name, led_driver, min_run_time, max_run_time, min_priority, max_priority);
}

void led_driver(void) {
		int i;
    led_clr_all();
    
    for(i=0; i<=red_height && i<NUM_LEDS; i++)
        led_set( red_ports[i], red_pins[i] );
    for(i=0; i<=green_height && i<NUM_LEDS; i++)
        led_set( green_ports[i], green_pins[i] );
}

void led_set( GPIO_TypeDef* led_port, unsigned int led ) {
    GPIO_SetBits( led_port, led ); // Spremeni stanje na LED
}
void led_clr( GPIO_TypeDef* led_port, unsigned int led  ) {
    GPIO_ResetBits( led_port, led ); // Spremeni stanje na LED
}
void led_toggle( GPIO_TypeDef* led_port, unsigned int led  ) {
    GPIO_ToggleBits( led_port, led ); // Spremeni stanje na LED
}

void led_clr_all() {
    int i;
    for(i=0; i<NUM_LEDS; i++) {
        led_clr( green_ports[i], green_pins[i] );
        led_clr( red_ports[i], red_pins[i] );
    }
}

void led_set_all() {
    int i;
    for(i=0; i<NUM_LEDS; i++) {
        led_set( green_ports[i], green_pins[i] );
        led_set( red_ports[i], red_pins[i] );
    }
}

void led_error_warning(int errno) {    
    int i;
    for(i=0; i<NUM_LEDS; i++) {
        led_set( red_ports[i], red_pins[i] );
        if(1<<i & errno)
            led_set( green_ports[i], green_pins[i]);
    }
}

void led_set_green_height( int h ) {green_height=h;}
void led_set_red_height( int h ) {red_height=h;}

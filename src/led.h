#include <stm32f4xx.h>

#include "gpio.h"
#include "os.h"

#define LED_NO_PIN_ON_PORT 0
#define NUM_LEDS 7

#define	LED_GREEN_0 GPIO_Pin_15	// PB15
#define LED_GREEN_0_PORT GPIOB
#define LED_GREEN_1 GPIO_Pin_8  // PD8
#define LED_GREEN_1_PORT GPIOD
#define LED_GREEN_2 GPIO_Pin_13 // PB13
#define LED_GREEN_2_PORT GPIOB
#define LED_GREEN_3 GPIO_Pin_14 // PB14
#define LED_GREEN_3_PORT GPIOB
#define LED_GREEN_4 GPIO_Pin_11 // PB11
#define LED_GREEN_4_PORT GPIOB
#define LED_GREEN_5 GPIO_Pin_10 // PB10
#define LED_GREEN_5_PORT GPIOB
#define LED_GREEN_6 GPIO_Pin_15 // PE15
#define LED_GREEN_6_PORT GPIOE

#define LED_RED_0 GPIO_Pin_13 // PE13
#define LED_RED_0_PORT GPIOE
#define LED_RED_1 GPIO_Pin_8  // PE8
#define LED_RED_1_PORT GPIOE
#define LED_RED_2 GPIO_Pin_11 // PE11
#define LED_RED_2_PORT GPIOE
#define LED_RED_3 GPIO_Pin_12 // PE12
#define LED_RED_3_PORT GPIOE
#define LED_RED_4 GPIO_Pin_9  // PE9
#define LED_RED_4_PORT GPIOE
#define LED_RED_5 GPIO_Pin_10 // PE10
#define LED_RED_5_PORT GPIOE
#define LED_RED_6 GPIO_Pin_7  // PE7
#define LED_RED_6_PORT GPIOE

#define LED_GPIOA_PINS LED_NO_PIN_ON_PORT
#define LED_GPIOB_PINS (LED_GREEN_0|LED_GREEN_2|LED_GREEN_3|LED_GREEN_4|LED_GREEN_5)
#define LED_GPIOC_PINS LED_NO_PIN_ON_PORT
#define LED_GPIOD_PINS (LED_GREEN_1)
#define LED_GPIOE_PINS (LED_GREEN_6|LED_RED_0|LED_RED_1|LED_RED_2|LED_RED_3|LED_RED_4|LED_RED_5|LED_RED_6)

void led_init( void );
void led_driver( void );

void led_set( GPIO_TypeDef*, unsigned int );
void led_clr( GPIO_TypeDef*, unsigned int );
void led_clr_all( void );
void led_set_all( void );
void led_toggle( GPIO_TypeDef*, unsigned int );
void led_error_warning( int errno );

void led_set_green_height( int );
void led_set_red_height( int );

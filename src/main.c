#include "main.h"
//#define OLD 1
//#define DBG 1

/*
    Datoteka glavnega programa
    Definiranih je nekaj 3D tabel za vsa stanja in funkcije menija
    V glavni zanki funkcije main je podpora za morebitni reset, obdelava podatkov za regulacijo
    	in pridobivanje vnosnih podatkov od uporabnika
*/

int get_menu_len[3] = {4, 3, 7};
int ts_data[TS_NUM];

char* frstLine;
char* lastLine;
char* lastLine_buffer;

int menu_h, menu_w, menu_w_up, menu_w_last;
int is_cooking, pwr_level;

/* Menu first line display strings */

char* main_menu[4] = {
	"Run/Stop",
	"View Status",
	"Config PID",
	"About"
};
char* view_menu[3] = {
	"Temperature",
	"PowerOut",
	"Back"
};
char* config_menu[7] = {
	"T target",
	"P factor",
	"I factor",
	"D factor",
	"H factor",
	"M factor",
	"Back"
};
char** curr_menu[3] = {
	main_menu,
	view_menu,
	config_menu
};

/* Functions for menus */
void f_placeholder( void ) {}

void f_run_stop( void ) {is_cooking = ((is_cooking>0)+1)%2;}

void (*main_menu_f[4]) ( void ) = {
	f_run_stop,
	f_placeholder,
	f_placeholder,
	f_placeholder
};

void (*view_menu_f[3]) ( void ) = {
	f_placeholder,
	f_placeholder,
	f_placeholder
};

void f_target_incr( void ) {int d = (menu_w_last > menu_w) ? -1 : 1; alg_set_target( (alg_get_target() + d) );}
void f_p_fac_incr( void ) {int d = (menu_w_last > menu_w) ? -1 : 1; alg_set_fac_p( (alg_get_fac_p() + d) );}
void f_i_fac_incr( void ) {int d = (menu_w_last > menu_w) ? -1 : 1; alg_set_fac_i( (alg_get_fac_i() + d) );}
void f_d_fac_incr( void ) {int d = (menu_w_last > menu_w) ? -1 : 1; alg_set_fac_d( (alg_get_fac_d() + d) );}
void f_h_fac_incr( void ) {int d = (menu_w_last > menu_w) ? -1 : 1; alg_set_fac_h( (alg_get_fac_h() + d) );}
void f_m_fac_incr( void ) {int d = (menu_w_last > menu_w) ? -1 : 1; alg_set_fac_m( (alg_get_fac_m() + d) );}

void (*config_menu_f[7]) ( void ) = {
	f_target_incr,
	f_p_fac_incr,
	f_i_fac_incr,
	f_d_fac_incr,
	f_h_fac_incr,
	f_m_fac_incr,
	f_placeholder
};

void (**curr_menu_f[3]) ( void ) = {
	main_menu_f,
	view_menu_f,
	config_menu_f
};

/* functions for changing last line of menu display */

// General templating function
#define NO_NUM -999
void d_template( char* str, int num ) {
	int len = strlen(str);
	strcpy( lastLine_buffer, str );
	if(num != NO_NUM)
		int2str( &lastLine_buffer[len], num, 0 );
}

void d_run_stop( void ) {
	if(is_cooking)
		d_template( "Cooking...", NO_NUM );
	else
		d_template( "Waiting...", NO_NUM );
}
void d_about( void ) {
	d_template( "Vinokuh by M&R", NO_NUM );
}

void (*main_menu_d[4]) ( void ) = {
	d_run_stop,
	f_placeholder,
	f_placeholder,
	d_about
};

void d_temperature( void ) {
	d_template( "T:", alg_get_curr_t() );
}
void d_power_out( void ) {
	d_template( "(0-6):", pwr_get_setting() );
}

void (*view_menu_d[3]) ( void ) = {
	d_temperature,
	d_power_out,
	f_placeholder
};

void d_check_menu_last_w( void ) {
	if(menu_w_last > menu_w) {lastLine_buffer[1] = 'd'; lastLine_buffer[2] = 'e';};
}

void d_target_incr( void ) {d_template( "(incr):", alg_get_target() );	d_check_menu_last_w();}
void d_p_fac_incr( void ) {d_template( "(incr):", alg_get_fac_p() );	d_check_menu_last_w();}
void d_i_fac_incr( void ) {d_template( "(incr):", alg_get_fac_i() );	d_check_menu_last_w();}
void d_d_fac_incr( void ) {d_template( "(incr):", alg_get_fac_d() );	d_check_menu_last_w();}
void d_h_fac_incr( void ) {d_template( "(incr):", alg_get_fac_h() );	d_check_menu_last_w();}
void d_m_fac_incr( void ) {d_template( "(incr):", alg_get_fac_m() );	d_check_menu_last_w();}

void (*config_menu_d[7]) ( void ) = {
	d_target_incr,
	d_p_fac_incr,
	d_i_fac_incr,
	d_d_fac_incr,
	d_h_fac_incr,
	d_m_fac_incr,
	f_placeholder
};

void (**curr_menu_d[3]) ( void ) = {
	main_menu_d,
	view_menu_d,
	config_menu_d
};

int get_change_menu[][7] = {
	{-1,  1,  2, -1, -1, -1, -1},
	{-1, -1,  0, -1, -1, -1, -1},
	{-1, -1, -1, -1, -1, -1,  0}
};

void reset() {
	menu_h = 0;
	menu_w = 0;

	display_flush();
	display_init();
	display_clrscr();

	show_ts_hold();

	display_goto(15,1);
	display_putch('0' + pwr_level);
}

void show_firstLine() {
	int i, l, s;

	l = strlen(curr_menu[menu_h][menu_w]);
	s = 7 - (l-1)/2;

	frstLine[0] = '<';
	frstLine[15] = '>';

	for(i=1; i<15; i++) {
		if( i<s || i>=s+l)
			frstLine[i] = ' ';
		else
			frstLine[i] = curr_menu[menu_h][menu_w][i-s];
	}

	display_goto(0,0);
	display_putstr(frstLine);
}

void show_lastLine() {
	int i, l, s;

	l = strlen(lastLine_buffer);
	s = 7 - (l-1)/2;

	for(i=0; i<16; i++) {
		if( i<s || i>=s+l)
			lastLine[i] = ' ';
		else
			lastLine[i] = lastLine_buffer[i-s];
	}

	#ifdef DBG
		lastLine[0] = '0' + menu_h;
		lastLine[1] = '0' + menu_w;
	#endif

	display_goto(0,1);
	display_putstr(lastLine);

	// clr buffer
	lastLine_buffer[0] = 0;
}

int main(void){

	int btn_l, btn_r, btn_c, redrawFrst, redrawLast, i;
	frstLine = (char*)malloc(16*sizeof(char));
	lastLine = (char*)malloc(16*sizeof(char));
	lastLine_buffer = (char*)malloc(16*sizeof(char));
	
	redrawFrst = 1;
	redrawLast = 1;
	is_cooking = 0;
	pwr_level = 0;
	
	// **************** INIT ******************
	ts_init();
	err_led_init();
	//encoder_init();
	pwr_init();
	display_init();
	led_init();
	input_init();
	alg_init();
	
	os_init();
	
	// **************** true stuff ******************
	
	//pwr_set_setting(1);
	
	while(1)
	{
		delay_ms(100);

		/* RESET */

		if(input_is_press_down(BTN_L) && input_is_press_down(BTN_R))
		{
			reset();			
			while(!input_is_press_up(BTN_L)) {delay_ms(1);}
			while(!input_is_press_up(BTN_R)) {delay_ms(1);}
			redrawFrst = 1;
			redrawLast = 1;
		}

		/* OPERATION LOGIC */

		// get values from sensors
	    for(i=0; i<TS_NUM; i++)
	        ts_data[i] = get_ts(TS0 + i);

		// calculate factors
		pwr_level = alg_calc(ts_data);
		pwr_level = PWR_MAX_LEVEL; // TODO: delete after regulation implementation

		// set pwr setting according to is_cooking
		if(is_cooking == 1)
			pwr_set_setting(pwr_level);
		else if (is_cooking == 0)
			pwr_set_setting(0);

		// set leds according to is_cooking, pwr_level and height
		led_set_red_height((is_cooking)?pwr_level:0);
		led_set_green_height(alg_get_height());

		/* INPUT */

		btn_l = input_is_press_up(BTN_L);
		btn_r = input_is_press_up(BTN_R);
		btn_c = input_is_press_up(BTN_C);
		

		if(btn_l | btn_r) 
		{
			// remember last menu_w if in config menu
			if(menu_h == 2)
				menu_w_last = menu_w % get_menu_len[menu_h];

			menu_w = (menu_w + (btn_r > 0) - (btn_l > 0) + get_menu_len[menu_h]) % get_menu_len[menu_h];
			redrawFrst = 1;
			redrawLast = 1;
		}

		if(btn_c)
		{
			redrawLast = 1;

			if(get_change_menu[menu_h][menu_w] != -1) {
				// remember old menu_w
				if(menu_h == 0)
					menu_w_up = menu_w;

				menu_h = get_change_menu[menu_h][menu_w];
				
				// restore old menu_w
				if(menu_h == 0)
					menu_w = menu_w_up;
				else
					menu_w = 0;

				redrawFrst = 1;
			}
			else {
				// else call func
				curr_menu_f[menu_h][menu_w]();
			}
		}

		if (redrawFrst)
		{
			show_firstLine();
			redrawFrst = 0;
		}

		if(redrawLast)
		{
			curr_menu_d[menu_h][menu_w]();
			show_lastLine();
			redrawLast = 0;
		}
	}
}

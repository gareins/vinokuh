#include <stm32f4xx.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "alg.h"
#include "ts.h"
#include "encoder.h"
#include "display.h"
#include "led.h"
#include "pwr.h"
#include "error.h"
#include "os.h"
#include "input.h"

void show_time( void );
void show_enc( void );
void show_ts( void );
void show_ts_hold( void );
void show_input( void );
void show_leds( void );
void show_pwr( void );
void main_old( void );

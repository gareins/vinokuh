#include "os.h"

/*
	Knjiznjica za operacijski sistem
*/

/***************************/
/******* CLOCK Stuff *******/
/***************************/

unsigned int clockCounter; 

// v mili sekundah - if 0 -> OS not initialized
// can run for ~ 6 * 10e8 years :)
uint64_t os_clock = 0;

uint64_t getOsClock(void)
{
	return os_clock + ((int)TIM2->CNT)/1000;
}

uint64_t getOsClock_us(void)
{
	return os_clock*1000 + TIM2->CNT;
}

void delay_us(uint64_t time) 
{
	//if OS not yet intialized
	if( os_clock == 0 )
	{
		SysTick_Config(SystemCoreClock/1000000);
		clockCounter = time;
		while(clockCounter != 0);
	}
	else
	{
		uint64_t t = getOsClock_us();
		while(getOsClock_us()-t < time);
	}
}

void delay_ms(uint64_t time) 
{
	//if OS not yet intialized
	if( os_clock == 0 )
	{
		SysTick_Config(SystemCoreClock/1000);
		clockCounter = time;
		while(clockCounter != 0);
	}
	else
	{
		uint64_t t = getOsClock();
		while(getOsClock()-t < time);
	}
}

// Private
void SysTick_Handler()
{
	if( clockCounter != 0 )
		clockCounter--;
}

/***************************/
/**** Private OS Stuff *****/
/***************************/

//driver properties
struct driver_prop
{
	char			name[16];			      // za debug
	void			(*driver)(void);	  // funkcija, ki jo klicemo

	unsigned int	min_run_time;		// min ali max casa, ko se mora...
	unsigned int	max_run_time;		// ... driver ponovno izvesti (v )
	unsigned int 	last_run_time;	// za izracun priorityja

	unsigned int	min_priority;		// za input visji, za lcd nizji (recimo)
	unsigned int 	max_priority;		// range: SCHEDULER_MIN_ -> _MAX_PRIORITY
};
typedef struct driver_prop driver_prop;

int os_num_of_drivers = 0;// stevilo VSEH driverjev
int os_running_driver = OS_NO_ACTIVE_DRIVER; // trenutno izvajajoci se driver

int osErrno; 
/* 0 ok,
 * 11 -> too many drivers
 * 12 -> scheduler error
 * 13 -> drive hung
 * 14,15 -> undefined
*/

driver_prop OS_DRIVERS[OS_MAX_DRIVERS];

int calculate_priority(int i);
void schedule_and_run(void);
int calculate_priority(int i);

void TIM2_IRQHandler(unsigned int par)
{
	if(TIM_GetITStatus(TIM2, TIM_IT_Update) != RESET)
	{
		TIM_ClearITPendingBit(TIM2, TIM_IT_Update);
		
		// OS ura se osvezi ob vsakem interuptu
		os_clock += OS_PERIOD_TIME;
		
		// ce smo zagnali prekinitev pred koncanjem prejsnjega driverja:
		if(os_running_driver != OS_NO_ACTIVE_DRIVER)
		{
			//TODO: lahk vnesemo kontrolo henganih driverjev 

			//pustimo, da tece naprej.
			return;
		}

		//drugace izberemo nov driver
		schedule_and_run();		
		return;
	}
}

void schedule_and_run(void)
{
	int i;
	int max_priority = SCHEDULER_DO_NOT_EXEC;
	int temp_priority;
	int driver_to_run;

	// O(n) casovna zahtevnost schedulerja
	for(i=0; i<os_num_of_drivers; i++)
	{
		temp_priority = calculate_priority(i);

		// ce je prioriteta tega driverja najvecja mozna, ga zazeni
		if(temp_priority == SCHEDULER_MAX_PRIORITY)
		{
			driver_to_run = i;
			break;
		}

		// drugace iscemo najvecjo prioriteto
		if(temp_priority < max_priority)
		{
			max_priority = temp_priority;
			driver_to_run = i;
		}
	}
	
	//TODO: Remove...
	//ce nimamo nobenega driverja za izvajat:
	if(max_priority == SCHEDULER_DO_NOT_EXEC)
		return;
	
	//naznanimo tekoci driver
	os_running_driver = driver_to_run;
	
	//zabelezimo zadnji cas izvajanja
	OS_DRIVERS[driver_to_run].last_run_time = getOsClock();

	//zazenemo driver
	(*(OS_DRIVERS[driver_to_run].driver))();

	//naznanimo koncanje driverja
	os_running_driver = OS_NO_ACTIVE_DRIVER;
}

int calculate_priority(int i)
{
	driver_prop* props = &OS_DRIVERS[i];
	int priority = props->min_priority;
	float T,dt,dp;
	// casa od zadnje izvedbe
	int t = os_clock - props->last_run_time;
	
	//ce je T manjsi od minimuma driverja ne izvajamo
	if(t <= props->min_run_time) 
		priority = SCHEDULER_DO_NOT_EXEC;

	//ce je ze prevec casa od zadnje izvedbe (T), potem ostane maximaln priority za driver
	else if(t >= props->max_run_time) 
		{;}

	//drugace izracunamo
	else
	{
		T  = (float) (props->max_run_time);
		dt = (float) (props->max_run_time - props->min_run_time);
		dp = (float) (props->min_priority - props->max_priority);

		/* relativno gledano koliko sem ze blizu max_run_time
		pomnozeno z vrednostjo, za kolikor lahko zbijemo priority */
		priority += (int)( (T-t)/dt * dp );
	}
	
	
	//sanity_check
	if( (priority != SCHEDULER_DO_NOT_EXEC) && ((priority > SCHEDULER_MIN_PRIORITY) || (priority < SCHEDULER_MAX_PRIORITY)) )
	{
		osErrno = 2;
		return SCHEDULER_DO_NOT_EXEC;
	}

	return priority;
}

/*****************************/
/****** Public OS stuff ******/
/*****************************/

void os_init(void)
{		
	NVIC_InitTypeDef NVIC_InitStructure;
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	
	/* Enable the TIM2 gloabal Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	
	/* TIM2 clock enable */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
	/* Time base configuration */
	TIM_TimeBaseStructure.TIM_Period = ((int)CNTS_PER_PERIOD) - 1; // 1 MHz down to 1 KHz (1 ms)
	TIM_TimeBaseStructure.TIM_Prescaler = ((int)US_COUNT+1) - 1; // 24 MHz Clock down to 1 MHz (adjust per your clock)
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure);
	
	/* TIM IT enable */
	TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);
	/* TIM2 enable counter */
	TIM_Cmd(TIM2, ENABLE);
	/* announce OS initialised */
	os_clock = 1;
}

void addDriver(	char* name, 
				void (*driver)(void), 
				unsigned int min_run_time, 
				unsigned int max_run_time, 
				unsigned int min_priority, 
				unsigned int max_priority)
{	
	driver_prop* props;

	if(os_num_of_drivers > OS_MAX_DRIVERS) {
		//error
		osErrno = 1;
		return;
	}	
	else {
		props = &OS_DRIVERS[os_num_of_drivers];
	}

	strncpy(props->name, name, 16);
	props->last_run_time = 0;
	props->min_run_time  = min_run_time;
	props->max_run_time  = max_run_time;
	props->min_priority  = min_priority;
	props->max_priority  = max_priority;
	props->driver        = driver;

	//dodali smo nov driver
	os_num_of_drivers++;
}

int getNumberOfDrivers(void)
{
	return os_num_of_drivers;
}

int getOsErrno(void)
{
	return osErrno;
}

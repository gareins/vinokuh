#include <stm32f4xx.h>
#include <string.h>

#define OS_SECOND 1000
#define OS_MILISECOND 1

#define US_COUNT 84 - 1
#define MS_COUNT 84000 - 1

#define OS_MAX_DRIVERS 64
#define OS_NO_ACTIVE_DRIVER OS_MAX_DRIVERS+1

#define OS_PERIOD_TIME 20
#define CNTS_PER_PERIOD (OS_PERIOD_TIME*1000) - 1

#define SCHEDULER_MIN_PRIORITY 255
#define SCHEDULER_MAX_PRIORITY 0
#define SCHEDULER_DO_NOT_EXEC SCHEDULER_MIN_PRIORITY+1

void os_init(void);
void addDriver(	char* name, 
				void (*driver)(void), 
				unsigned int min_run_time, 
				unsigned int max_run_time, 
				unsigned int min_priority, 
				unsigned int max_priority);

uint64_t getOsClock(void);
uint64_t getOsClock_us(void);								
								
int getNumberOfDrivers(void);
int getOsErrno(void);
								
void delay_ms(uint64_t time);
void delay_us(uint64_t time);

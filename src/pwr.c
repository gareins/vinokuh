#include "pwr.h"

/*
	Driver za priziganje grelca preko relejev
*/

static char				name[16] = "pwr_driver";
static unsigned int min_run_time = OS_SECOND*5;
static unsigned int max_run_time = OS_SECOND*8;
static unsigned int min_priority = 100;
static unsigned int max_priority = 50;

void pwr_init( void ) {
	RCC_AHB1PeriphClockCmd(PWR_0_CLK, ENABLE);
	RCC_AHB1PeriphClockCmd(PWR_1_CLK, ENABLE);

	gpio_init(PWR_0_PIN, GPIO_Mode_OUT, GPIO_OType_OD, GPIO_PuPd_UP, GPIO_Speed_100MHz, PWR_0_PORT);
	gpio_init(PWR_1_PIN, GPIO_Mode_OUT, GPIO_OType_OD, GPIO_PuPd_UP, GPIO_Speed_100MHz, PWR_1_PORT);

	GPIO_ResetBits(PWR_0_PORT, PWR_0_PIN);
	GPIO_ResetBits(PWR_1_PORT, PWR_1_PIN);

	addDriver(name, pwr_driver, min_run_time, max_run_time, min_priority, max_priority);
}

unsigned int pwr_levels[][2] = {
	{PWR_0000, PWR_0000},
	{PWR_0000, PWR_0500},
	{PWR_0500, PWR_0500},
	{PWR_0500, PWR_1000},
	{PWR_1000, PWR_1000},
	{PWR_1000, PWR_1500},
	{PWR_1500, PWR_1500}
};

unsigned int setting;
unsigned int curr_open;
unsigned int curr_idx;

void pwr_driver(void) {
	unsigned int next_open, to_close , to_open;

	next_open = pwr_levels[setting][curr_idx];
	to_close  = (curr_open ^ next_open) & curr_open;
	to_open   = (curr_open ^ next_open) & next_open;

	if(to_close & PWR_0500)
		GPIO_ResetBits(PWR_0_PORT, PWR_0_PIN);
	if(to_close & PWR_1000)
		GPIO_ResetBits(PWR_1_PORT, PWR_1_PIN);

	if(to_open & PWR_0500)
		GPIO_SetBits(PWR_0_PORT, PWR_0_PIN);
	if(to_open & PWR_1000)
		GPIO_SetBits(PWR_1_PORT, PWR_1_PIN);

	curr_idx = (curr_idx+1)%2;
	curr_open = next_open;
}

void pwr_set_setting(unsigned int s)
{
	setting = s>6 ? 0 : s;
}

unsigned int pwr_get_setting( void )
{
	return setting;
}

// TODO: on/off function

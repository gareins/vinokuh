#include <stm32f4xx.h>

#include "gpio.h"
#include "os.h"

#define PWR_0_PIN       GPIO_Pin_14
#define PWR_0_PORT      GPIOC
#define PWR_0_CLK       RCC_AHB1Periph_GPIOC

#define PWR_1_PIN       GPIO_Pin_6
#define PWR_1_PORT      GPIOE
#define PWR_1_CLK       RCC_AHB1Periph_GPIOE

#define PWR_0000 0x00
#define PWR_0500 0x01
#define PWR_1000 0x02
#define PWR_1500 (PWR_0500 | PWR_1000)

#define PWR_LEVELS_NUM 7
#define PWR_MAX_LEVEL 6

void pwr_init( void );
void pwr_driver(void);
void pwr_set_setting(unsigned int s);
unsigned int pwr_get_setting( void );

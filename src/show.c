#include "main.h"

/*
    Knjiznjica funkcij za prikaz delovanja in testiranje
*/

//works for 50 days :)
void show_time()
{
    char c[10];
    int t = getOsClock();
    
    int2str(&c[0], (((t/1000)/60)/60)%24, 2);
    c[2] = '-';
    int2str(&c[3], ((t/1000)/60)%60, 2);
    c[5] = '-';
    int2str(&c[6], (t/1000)%60, 2);
    c[8] = '.';
    int2str(&c[9], t/100, 1);    
    
    display_goto(0,0);
    display_putstr(c);  
}

int cntr = 0;

void show_enc()
{
    char str[32];
    int a,b,c,d;
    
    cntr++;
    
    if(1)
    {
        encoder_info(&a, &b, &c, &d);
        int2str(&str[0], a, 3);
        str[3] = ' ';
        int2str(&str[4], b, 3);
        str[7] = ' ';
        int2str(&str[8], c, 3);
        str[11] = ' ';
        int2str(&str[12], cntr, 2); 
        int2str(&str[16], d, 3);
    }
    else
    {
        a = encoder_read();
        if( a > 0)
            str[0] = '>';
        else if ( a < 0 )
            str[0] = '<';
        else
            str[0] = '-';
        
        str[1] = 0;
    }
    
    display_goto(0,0);
    display_putstr(str);    
}

void show_ts()
{
    char str[16];
    int a;
    
    int i;
    for(i=0; i<TS_NUM; i++){
        a = get_ts(TS0 + i);

        str[0] = '0' + i;
        str[1] = ':';
                
        int2str(&str[2], a, 4);

        display_goto(0,0);
        display_putstr(str);
        delay_ms(1000);
    }       
}

void show_ts_hold()
{
    char str[28];
    int a, i;
    int ts_data[TS_NUM];

    // get values from sensors
    for(i=0; i<TS_NUM; i++)
        ts_data[i] = get_ts(TS0 + i);

    // calculate temperatures
    a = alg_calc( ts_data );

    // get temperatures    
    for(i=0; i<TS_NUM; i++){
        a = alg_get_t_cal( i );

        str[4*i - 1] = ' ';
        int2str(&str[4*i], a, 3);
    }
    display_goto(0,0);
    display_putstr(str);  
    display_goto(0,1);
    display_putstr(&str[16]);
}

void show_input()
{
    display_goto(0,0);
    display_putch('0' + input_is_press_up(BTN_L));
    
    display_goto(2,0);
    display_putch('0' + input_is_press_up(BTN_C));
    
    display_goto(4,0);
    display_putch('0' + input_is_press_up(BTN_R));
}

#define LED_DELAY 500

void show_leds()
{
    led_clr_all();
    delay_ms(LED_DELAY);
    
    led_toggle(LED_GREEN_0_PORT, LED_GREEN_0);
    led_toggle(LED_RED_0_PORT, LED_RED_0);
    delay_ms(LED_DELAY);

    led_toggle(LED_GREEN_1_PORT, LED_GREEN_1);
    led_toggle(LED_RED_1_PORT, LED_RED_1);
    delay_ms(LED_DELAY);

    led_toggle(LED_GREEN_2_PORT, LED_GREEN_2);
    led_toggle(LED_RED_2_PORT, LED_RED_2);
    delay_ms(LED_DELAY);

    led_toggle(LED_GREEN_3_PORT, LED_GREEN_3);
    led_toggle(LED_RED_3_PORT, LED_RED_3);
    delay_ms(LED_DELAY);

    led_toggle(LED_GREEN_4_PORT, LED_GREEN_4);
    led_toggle(LED_RED_4_PORT, LED_RED_4);
    delay_ms(LED_DELAY);

    led_toggle(LED_GREEN_5_PORT, LED_GREEN_5);
    led_toggle(LED_RED_5_PORT, LED_RED_5);
    delay_ms(LED_DELAY);

    led_toggle(LED_GREEN_6_PORT, LED_GREEN_6);
    led_toggle(LED_RED_6_PORT, LED_RED_6);
    delay_ms(LED_DELAY);
    
    //led_set_all();
    delay_ms(LED_DELAY);
}

void show_pwr()
{
    GPIO_SetBits(PWR_0_PORT, PWR_0_PIN);
    delay_ms(1000);
    GPIO_SetBits(PWR_1_PORT, PWR_1_PIN);
    delay_ms(2000);
    GPIO_ResetBits(PWR_0_PORT, PWR_0_PIN);
    delay_ms(1000);
    GPIO_ResetBits(PWR_1_PORT, PWR_1_PIN);
}

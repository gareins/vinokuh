#include "ts.h"

/*
	Driver za temperaturne senzorje preko ADC
	Uporablja se ADC1
*/

static char				 	name[16] = "ts_driver";
static unsigned int min_run_time = OS_MILISECOND*150;
static unsigned int max_run_time = OS_MILISECOND*200;
static unsigned int min_priority = 100;
static unsigned int max_priority = 20;

void ts_init() {
	ADC_CommonInitTypeDef ADC_CommonInitStruct;
	ADC_InitTypeDef ADC_InitStruct;
	
	// Enable the clock for ADC
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
	
	// Nastavi pine za temp senzorje
	ts_pin_init();
	
  // Common ADC init sets the prescaler
	ADC_CommonStructInit(&ADC_CommonInitStruct);
	ADC_CommonInitStruct.ADC_Prescaler = ADC_Prescaler_Div4;
	ADC_CommonInit(&ADC_CommonInitStruct);

  /* ADC1 Configuration */
	ADC_StructInit(&ADC_InitStruct);
	ADC_InitStruct.ADC_Resolution = ADC_Resolution_10b;
	ADC_Init(ADC1, &ADC_InitStruct);
	ADC_Cmd(ADC1, ENABLE);	

  /* Now do the setup */
  ADC_Init(ADC1, &ADC_InitStruct);
  /* Enable ADC1 */
  ADC_Cmd(ADC1, ENABLE);
	
	//ADC_RegularChannelConfig(ADC1, 10, 1, ADC_SampleTime_15Cycles);

  /* Enable ADC1 reset calibration register */
  //ADC_ResetCalibration(ADC1);
  /* Check the end of ADC1 reset calibration register */
  //while(ADC_GetResetCalibrationStatus(ADC1));
  /* Start ADC1 calibaration */
  //ADC_StartCalibration(ADC1);
  /* Check the end of ADC1 calibration */
  //while(ADC_GetCalibrationStatus(ADC1));
	
	addDriver(name, ts_driver, min_run_time, max_run_time, min_priority, max_priority);
}

void ts_pin_init(){
    GPIO_InitTypeDef GPIO_InitStruct;
    GPIO_StructInit(&GPIO_InitStruct); // Reset gpio init structure
    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AIN; // Obvezno AIN !!!

    RCC_AHB1PeriphClockCmd(TS0_CLK, ENABLE);
    GPIO_InitStruct.GPIO_Pin = (TS0_PIN);
    GPIO_Init(TS0_PORT, &GPIO_InitStruct);

    RCC_AHB1PeriphClockCmd(TS1_CLK, ENABLE);
    GPIO_InitStruct.GPIO_Pin = (TS1_PIN);
    GPIO_Init(TS1_PORT, &GPIO_InitStruct);
	
		RCC_AHB1PeriphClockCmd(TS2_CLK, ENABLE);
    GPIO_InitStruct.GPIO_Pin = (TS2_PIN);
    GPIO_Init(TS2_PORT, &GPIO_InitStruct);

    RCC_AHB1PeriphClockCmd(TS3_CLK, ENABLE);
    GPIO_InitStruct.GPIO_Pin = (TS3_PIN);
    GPIO_Init(TS3_PORT, &GPIO_InitStruct);
    
    RCC_AHB1PeriphClockCmd(TS4_CLK, ENABLE);
    GPIO_InitStruct.GPIO_Pin = (TS4_PIN);
    GPIO_Init(TS4_PORT, &GPIO_InitStruct);
    
    RCC_AHB1PeriphClockCmd(TS5_CLK, ENABLE);
    GPIO_InitStruct.GPIO_Pin = (TS5_PIN);
    GPIO_Init(TS5_PORT, &GPIO_InitStruct);
    
    RCC_AHB1PeriphClockCmd(TS6_CLK, ENABLE);
    GPIO_InitStruct.GPIO_Pin = (TS6_PIN);
    GPIO_Init(TS6_PORT, &GPIO_InitStruct);
    
    RCC_AHB1PeriphClockCmd(TS7_CLK, ENABLE);
    GPIO_InitStruct.GPIO_Pin = (TS7_PIN);
    GPIO_Init(TS7_PORT, &GPIO_InitStruct);
}

int ADC1_read(u8 channel) {
		ADC_RegularChannelConfig(ADC1, channel, 1, ADC_SampleTime_15Cycles);
		ADC_SoftwareStartConv(ADC1);
		while(ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) != SET);
		return ADC_GetConversionValue(ADC1);
}

int ts_read(u8 level){
    switch (level){
        case TS0: return ADC1_read(TS0_CHANNEL);
        case TS1: return ADC1_read(TS1_CHANNEL);
        case TS2: return ADC1_read(TS2_CHANNEL);
        case TS3: return ADC1_read(TS3_CHANNEL);
        case TS4: return ADC1_read(TS4_CHANNEL);
        case TS5: return ADC1_read(TS5_CHANNEL);
        case TS6: return ADC1_read(TS6_CHANNEL);
        case TS7: return ADC1_read(TS7_CHANNEL);
        default: return 0;
    }
}

/*********************** DRIVER **********************/
int TS_REG[TS_NUM];
int iTS = 0;

int get_ts(int TSX){
	return TS_REG[TSX];
}

void ts_driver() {
	TS_REG[iTS] = ts_read(iTS);
	iTS = (iTS + 1) % TS_NUM;
}

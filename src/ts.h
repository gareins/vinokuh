#include "stm32f4xx.h"

#include "os.h"

// #######################################
// ############## Public #################
// #######################################

// Pins
#define TS0	   0
#define TS1	   1
#define TS2	   2
#define TS3	   3
#define TS4	   4
#define TS5	   5
#define TS6	   6
#define TS7	   7
#define TS_NUM 7

// Functions
void    ts_init(void);
int     get_ts(int TSX);


// #######################################
// ############## Private ################
// #######################################
// ### do # not # use # outside # adc ####

// Pins

#define TS0_PIN     GPIO_Pin_2
#define TS0_PORT    GPIOC
#define TS0_CLK     RCC_AHB1Periph_GPIOC
#define TS0_CHANNEL	12

#define TS1_PIN     GPIO_Pin_0
#define TS1_PORT    GPIOA
#define TS1_CLK     RCC_AHB1Periph_GPIOA
#define TS1_CHANNEL	0

#define TS2_PIN     GPIO_Pin_2
#define TS2_PORT    GPIOA
#define TS2_CLK     RCC_AHB1Periph_GPIOA
#define TS2_CHANNEL	2

#define TS3_PIN     GPIO_Pin_4
#define TS3_PORT    GPIOA
#define TS3_CLK     RCC_AHB1Periph_GPIOA
#define TS3_CHANNEL	4

#define TS4_PIN     GPIO_Pin_6
#define TS4_PORT    GPIOA
#define TS4_CLK     RCC_AHB1Periph_GPIOA
#define TS4_CHANNEL	6

#define TS5_PIN     GPIO_Pin_4
#define TS5_PORT    GPIOC
#define TS5_CLK     RCC_AHB1Periph_GPIOC
#define TS5_CHANNEL	14

#define TS6_PIN     GPIO_Pin_0
#define TS6_PORT    GPIOB
#define TS6_CLK     RCC_AHB1Periph_GPIOB
#define TS6_CHANNEL	8

#define TS7_PIN     GPIO_Pin_1
#define TS7_PORT    GPIOB
#define TS7_CLK     RCC_AHB1Periph_GPIOB
#define TS7_CHANNEL	9

// Functions
void    ts_pin_init(void);
int     ADC1_read(u8 channel);
int     ts_read(u8 level);
void 	ts_driver(void);





